#include <stdio.h>
#include <stdlib.h>

// An example of using dynamic memory allocation to create and return an
// array from a function.

/**
 * Dynamically allocates (on the heap) an array of n elements
 * initialized to 0, ..., n-1.  If the array could not be allocated
 * then the return value is NULL.  It is the caller's responsibility
 * to free the array.
 *
 * @param n a nonegative integer
 * @return an array of n integers, or NULL
 */
int *make_array(int n);

int main(int argc, char *argv[])
{
  int *arr = make_array(15);
  if (arr != NULL)
    {
      for (int i = 0; i < 15; i++)
	{
	  // If arr had been allocated in the stack (declared with int arr[15])
	  // then using its name in *any* context, including [], works with
	  // a pointer to the 1st element.  The pointer to the array
	  // returned from make_array is already a pointer to the
	  // 1st element, so there is no need to change the syntax for
	  // accessing arrays using a pointer to them.
	  printf("%d ", arr[i]);
	}
      printf("\n");
    }

  // free the array (if arr == NULL this is still safe)
  free(arr);
}

int *make_array(int n)
{
  int *arr = malloc(sizeof(int) * n);
  if (arr != NULL)
    {
      // allocation was successful; initialize
      for (int i = 0; i < n; i++)
	{
	  // again, same syntax as for an array allocated on the stack
	  // (as would be the case for int arr[n])...
	  arr[i] = i;
	}
    }

  // ...but now, since the array arr points to is on the heap instead
  // of the the stack, the array will survive the end of the function
  // and so it is safe to return a pointer to it
  return arr;
}
