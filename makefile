CC = gcc
CFLAGS = -std=c99 -pedantic -Wall -g3

MakeArray: make_array.o
	${CC} ${CFLAGS} -o $@ $^
